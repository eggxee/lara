<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">

    <title>Narrow Jumbotron Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="../../../../dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
    <!-- Custom styles for this template -->
    <link href="narrow-jumbotron.css" rel="stylesheet">
  </head>

  <body>

    <div class="container">
      <div class="header clearfix">
        <nav>
          <ul class="nav nav-pills float-right">
           
          </ul>
        </nav>
      </div>
<?php
$con=mysqli_connect("localhost","root","1roman0","infinitymall");

// Check connection
if (mysqli_connect_errno())
{
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
}


?> 
      <div class="xjumbotron">
        <?php 
$sql="SELECT * FROM products ORDER BY id";
if ($result=mysqli_query($con,$sql))
{
        $i = 1;
       echo PHP_EOL;
        while ($obj=mysqli_fetch_object($result)) {
          $isLastRow = FALSE;
          
            if ($i == 1) {
              echo '<div class="row">';
            }

            echo '<div class="col-sm-4">';
            echo $obj->id. ' ' . $obj->price .  ' ' . $obj->name;//  . ' ' . $obj->description;
            echo '</div>';

          if ($i == 3) {
              echo '</div>' . PHP_EOL; //end div row
              $isLastRow = TRUE;
              $i = 1;
              continue;
          }

          $i++; 
        }
      if (!$isLastRow) //put end if not divisible by 3
          echo '</div>' . PHP_EOL; //end div row
        

  // Free result set
  mysqli_free_result($result);
}
mysqli_close($con);
        ?>

      </div><!--/jumbotron-->

      <footer class="footer">
       
      </footer>

    </div> <!-- /container -->

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../../../assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
