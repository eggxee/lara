<?php
  // Change these.
  $server    = "{imap.gmail.com:993/imap/ssl/novalidate-cert}";
  $email     = "@gmail.com";
  $password  = "";

  // The code assumes that the folders Test/Sub1/Sub11, etc. exist.
  $selected  = "{$server}Test/Sub1/Sub12";
  $conn      = imap_open($selected, $email , $password);

  // This returns the $specified mailbox and its sub mailboxes,
  // even if the $specified mailbox is outside the $selected mailbox.
  $specified =  "{$server}Test/Sub1";
  $boxes     = imap_list($conn, $specified , '*');
  print_r($boxes);
 
  // This appends the message in the $specified mailbox.
  // It ignores the $selected mailbox.
  imap_append($conn, $specified
                     , "From: me@example.com\r\n"
                     . "To: you@example.com\r\n"
                     . "Subject: test\r\n"
                     . "\r\n"
                     . "this is a test message, please ignore\r\n");

  // This changes the $selected mailbox
  $selected = "{$server}Test/Sub1";
  imap_reopen($conn, $selected);

  // This moves a message from the $selected to the $specified mailbox
  // In this case, the specified mailbox does not include the server.
  imap_mail_move ($conn , "1" ,  "Test");
  imap_expunge($conn);
  imap_close($conn);
  // If you executed this code with a real IMAP server,
  // the message is now in the Test mailbox !
?>